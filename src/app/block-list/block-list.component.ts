import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-block-list',
  templateUrl: './block-list.component.html',
  styleUrls: ['./block-list.component.css']
})
export class BlockListComponent {
  @Input() userEnter = '';
  messageUse = '';
  messageText = [
    {message: 'Сделать домашку'},
    {message: 'Убрать дом'},
    {message: 'Покормить кота'}
  ];

  addMess(event: Event){
    event.preventDefault();
    this.messageText.push({message: this.messageUse})
  }

  onDelete(index: number){
    this.messageText.splice(index, 1);
  }

  refactorMessage(index: number, newText: string){
    this.messageText[index].message = newText
  }

}
