import {Component, EventEmitter, Input, Output,} from '@angular/core';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent {
  @Input() message = '';
  @Input() userInter = '';
  @Output() delete = new EventEmitter();
  @Output() change = new EventEmitter<string>();

  onClickDelete() {
    this.delete.emit();
  }

  onChangeInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.change.emit(target.value);
  }

onClassChange(){
    return this.checkBox()
}
  checkBox() {
    const chb = <HTMLInputElement> document.querySelector('input[name="che"]');
    if (chb.checked){
      return 'blue';
    }else {
      return 'grey';
    }
  }
}
